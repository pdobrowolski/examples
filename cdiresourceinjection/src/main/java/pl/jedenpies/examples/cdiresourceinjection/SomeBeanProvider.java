package pl.jedenpies.examples.cdiresourceinjection;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class SomeBeanProvider {

	@Produces @RequestScoped
	public SomeBean produceSomeBean() {
		return new SomeBean();
	}
}
