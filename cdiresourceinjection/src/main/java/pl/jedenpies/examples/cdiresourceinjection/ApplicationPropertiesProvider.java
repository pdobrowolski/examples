package pl.jedenpies.examples.cdiresourceinjection;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

@ApplicationScoped
public class ApplicationPropertiesProvider {

	private static final String PROPERTY_FILE_NAME = "application.properties";
	
	private Properties readProperties = new Properties();

	@PostConstruct
	public void readProperties() {
		try {
			InputStream is =
				this.getClass().getClassLoader().getResourceAsStream(PROPERTY_FILE_NAME);
			if (is == null) return;		
			readProperties.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Produces @ApplicationProperty
	public String getAsString(InjectionPoint injectionPoint) {
		String propertyName = propertyNameFor(injectionPoint);
		String valueForFieldName = readProperties.getProperty(propertyName);
		return valueForFieldName == null ? "" : valueForFieldName;
	}
	
	@Produces @ApplicationProperty
	public Long getAsLong(InjectionPoint injectionPoint) {
		return Long.valueOf(getAsString(injectionPoint));
	}
	
	@Produces @ApplicationProperty
	public List<String> getAsList(InjectionPoint injectionPoint) {
		String asString = getAsString(injectionPoint);
		return Arrays.asList(asString.split(","));
	}
	
	private String propertyNameFor(InjectionPoint point) {
		return point.getAnnotated().getAnnotation(ApplicationProperty.class).value();
	}
}
