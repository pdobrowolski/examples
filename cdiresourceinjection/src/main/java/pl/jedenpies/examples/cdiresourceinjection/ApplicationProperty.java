package pl.jedenpies.examples.cdiresourceinjection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface ApplicationProperty {
	
	@Nonbinding
	public String value() default "";
	
	@Nonbinding
	public String collectionSeparator() default ","; 
}