package pl.jedenpies.examples.cdiresourceinjection;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

@SuppressWarnings("serial")
@Named @ViewScoped
public class SomeView implements Serializable {

	@Inject
	private SomeBean someBean;
	
	@Inject @ApplicationProperty("pl.jedenpies.examples.helloWorld")
	private String helloWorld;
	
	@Inject @ApplicationProperty("pl.jedenpies.examples.number")
	private Long number;
	
	@Inject @ApplicationProperty("pl.jedenpies.examples.names")
	private List<String> names;
	
	@Inject
	private Logger log;
	
	public SomeBean getSomeBean() {
		return someBean;
	}
	
	public String getHelloWorld() {
		return helloWorld;
	}
	
	public Long getNumber() {
		return number;
	}
	
	public List<String> getNames() {
		return names;
	}
}
