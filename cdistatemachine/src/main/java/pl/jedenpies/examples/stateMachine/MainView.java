package pl.jedenpies.examples.stateMachine;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.jedenpies.examples.stateMachine.Order.PaymentWay;

@SuppressWarnings("serial")
@Named @ViewScoped
public class MainView implements Serializable {

	@Inject private OrderStateMachine stateMachine;
	
	private PaymentWay paymentWay;
	private Order order;
	
	public List<PaymentWay> getAvailablePaymentWays() {
		return Arrays.asList(PaymentWay.values());
	}
	
	public void createOrder() {
		this.order = Order.with(paymentWay);
	}
	
	public void performTransition(OrderStateTransition transition) {
		stateMachine.performTransition(order, transition);
	}
	
	public List<OrderStateTransition> getAvailableTransitions() {
		return stateMachine.getHandler(order.getState()).getAvailableTransitions();
	}
	
	public PaymentWay getPaymentWay() {
		return paymentWay;
	}
	public void setPaymentWay(PaymentWay paymentWay) {
		this.paymentWay = paymentWay;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
}
