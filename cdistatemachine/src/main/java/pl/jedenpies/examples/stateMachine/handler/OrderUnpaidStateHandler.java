package pl.jedenpies.examples.stateMachine.handler;

import static pl.jedenpies.examples.stateMachine.OrderStateTransition.CANCEL;
import static pl.jedenpies.examples.stateMachine.OrderStateTransition.MARK_PAID;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import pl.jedenpies.examples.stateMachine.Order;
import pl.jedenpies.examples.stateMachine.OrderState;
import pl.jedenpies.examples.stateMachine.OrderStateTransition;

@ApplicationScoped
public class OrderUnpaidStateHandler extends OrderStateHandler {

	private static final List<OrderStateTransition> AVAILABLE_TRANSITIONS = Arrays.asList(MARK_PAID, CANCEL);
	
	@Override
	public List<OrderStateTransition> getAvailableTransitions() {
		return AVAILABLE_TRANSITIONS;
	}

	@Override
	public OrderState getStateHandled() {
		return OrderState.UNPAID;
	}

	@Override
	public void markPaid(Order order) {
		order.setState(OrderState.IN_DELIVERY);
	}
	
	@Override
	public void cancel(Order order) {
		order.setState(OrderState.CANCELED);
	}
}
