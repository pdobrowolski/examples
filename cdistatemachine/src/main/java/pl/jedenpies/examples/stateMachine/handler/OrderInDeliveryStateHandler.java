package pl.jedenpies.examples.stateMachine.handler;

import static pl.jedenpies.examples.stateMachine.OrderStateTransition.MARK_DELIVERED;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import pl.jedenpies.examples.stateMachine.Order;
import pl.jedenpies.examples.stateMachine.OrderState;
import pl.jedenpies.examples.stateMachine.OrderStateTransition;

@ApplicationScoped
public class OrderInDeliveryStateHandler extends OrderStateHandler {

	private static final List<OrderStateTransition> AVAILABLE_TRANSITIONS = Arrays.asList(MARK_DELIVERED);
	
	@Override
	public List<OrderStateTransition> getAvailableTransitions() {
		return AVAILABLE_TRANSITIONS;
	}

	@Override
	public OrderState getStateHandled() {
		return OrderState.IN_DELIVERY;
	}
	
	@Override
	public void markDelivered(Order order) {
		order.setState(OrderState.DELIVERED);
	}

	
}
