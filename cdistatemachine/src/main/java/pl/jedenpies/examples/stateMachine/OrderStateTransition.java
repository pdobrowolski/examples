package pl.jedenpies.examples.stateMachine;

import pl.jedenpies.examples.stateMachine.handler.OrderStateHandler;

public enum OrderStateTransition {
	
	CANCEL((h, o) -> h.cancel(o)),
	CONFIRM((h, o) -> h.confirm(o)),
	MARK_PAID((h, o) -> h.markPaid(o)),
	MARK_DELIVERED((h, o) -> h.markDelivered(o));
	
	private OrderStateConsumerAction action;
	
	private OrderStateTransition(OrderStateConsumerAction action) {
		this.action = action;
	}
	
	public void execute(OrderStateHandler handler, Order order) {
		this.action.execute(handler, order);
	}
	
	@FunctionalInterface
	private interface OrderStateConsumerAction {		
		public void execute(OrderStateHandler handler, Order order);
	}
}
