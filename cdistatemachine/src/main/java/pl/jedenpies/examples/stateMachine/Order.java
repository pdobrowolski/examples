package pl.jedenpies.examples.stateMachine;

public class Order {

	private OrderState state;
	private PaymentWay paymentWay;
	
	private Order(PaymentWay paymentWay) {
		this.state = OrderState.PLACED;
		this.paymentWay = paymentWay;
	}
	
	public static Order with(PaymentWay paymentWay) {
		return new Order(paymentWay);
	}
	
	public void setState(OrderState state) {
		this.state = state;
	}
	public OrderState getState() {
		return state;
	}
	public void setPaymentWay(PaymentWay paymentWay) {
		this.paymentWay = paymentWay;
	}
	public PaymentWay getPaymentWay() {
		return paymentWay;
	}
	
	public enum PaymentWay {
		CASH, BANK_TRANSFER;
	}
}
