package pl.jedenpies.examples.stateMachine;

public enum OrderState {
	PLACED,
	UNPAID,
	IN_DELIVERY,
	DELIVERED,
	CANCELED
}
