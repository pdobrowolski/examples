package pl.jedenpies.examples.stateMachine.handler;

import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import pl.jedenpies.examples.stateMachine.OrderState;
import pl.jedenpies.examples.stateMachine.OrderStateTransition;

@ApplicationScoped
public class OrderDeliveredStateHandler extends OrderStateHandler {

	@Override
	public List<OrderStateTransition> getAvailableTransitions() {
		return Collections.emptyList();
	}

	@Override
	public OrderState getStateHandled() {
		return OrderState.DELIVERED;
	}
}
