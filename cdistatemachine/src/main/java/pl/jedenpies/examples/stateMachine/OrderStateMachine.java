package pl.jedenpies.examples.stateMachine;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import pl.jedenpies.examples.stateMachine.handler.OrderStateHandler;

@ApplicationScoped
public class OrderStateMachine {

	private Map<OrderState, OrderStateHandler> handlers = new HashMap<>();

	public OrderStateMachine() {}
	
	@Inject
	public OrderStateMachine(Instance<OrderStateHandler> handlers) {
		handlers.forEach(h -> this.handlers.put(h.getStateHandled(), h));
	}
	
	public void performTransition(Order order, OrderStateTransition transition) {
		getHandler(order.getState()).performTransition(order, transition);
	}
	
	public OrderStateHandler getHandler(OrderState state) {
		return Optional.ofNullable(handlers.get(state)).orElseGet(EmptyHandler::new);
	}
	
	private class EmptyHandler extends OrderStateHandler {

		@Override
		public List<OrderStateTransition> getAvailableTransitions() {
			return Collections.emptyList();
		}

		@Override
		public OrderState getStateHandled() {
			return null;
		}
		
	}
}
