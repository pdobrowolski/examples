package pl.jedenpies.examples.stateMachine.handler;

import java.util.List;

import pl.jedenpies.examples.stateMachine.InvalidTransitionException;
import pl.jedenpies.examples.stateMachine.Order;
import pl.jedenpies.examples.stateMachine.OrderState;
import pl.jedenpies.examples.stateMachine.OrderStateTransition;

public abstract class OrderStateHandler {

	public abstract List<OrderStateTransition> getAvailableTransitions();
	public abstract OrderState getStateHandled();
	
	public void performTransition(Order order, OrderStateTransition transition) {		
		transition.execute(this, order);
	}
	
	public void confirm(Order order) {
		throw new InvalidTransitionException();
	}
	
	public void markPaid(Order order) {
		throw new InvalidTransitionException();
	}
	
	public void markDelivered(Order order) {
		throw new InvalidTransitionException();
	}
	
	public void cancel(Order order) {
		throw new InvalidTransitionException();
	}
}
