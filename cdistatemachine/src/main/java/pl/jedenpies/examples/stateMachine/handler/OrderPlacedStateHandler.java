package pl.jedenpies.examples.stateMachine.handler;

import static pl.jedenpies.examples.stateMachine.OrderStateTransition.CANCEL;
import static pl.jedenpies.examples.stateMachine.OrderStateTransition.CONFIRM;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import pl.jedenpies.examples.stateMachine.Order;
import pl.jedenpies.examples.stateMachine.Order.PaymentWay;
import pl.jedenpies.examples.stateMachine.OrderState;
import pl.jedenpies.examples.stateMachine.OrderStateTransition;;

@ApplicationScoped
public class OrderPlacedStateHandler extends OrderStateHandler {
	
	private static final List<OrderStateTransition> AVAILABLE_TRANSITIONS = Arrays.asList(CONFIRM, CANCEL);
	
	@Override
	public List<OrderStateTransition> getAvailableTransitions() {
		return AVAILABLE_TRANSITIONS;
	}
	
	@Override
	public OrderState getStateHandled() {
		return OrderState.PLACED;
	}
	
	@Override
	public void confirm(Order order) {
		order.setState(
			order.getPaymentWay() == PaymentWay.CASH 
			? OrderState.IN_DELIVERY 
			: OrderState.UNPAID);		
	}
	
	@Override
	public void cancel(Order order) {
		order.setState(OrderState.CANCELED);
	}
}
